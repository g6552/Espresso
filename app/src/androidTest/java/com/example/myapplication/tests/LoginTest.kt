package com.example.myapplication.tests
import com.example.myapplication.pages.LoginPage

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.example.myapplication.R
import com.example.myapplication.ui.login.LoginActivity
import org.junit.Rule
import org.junit.Test
import java.lang.Thread.sleep

class LoginTest {

    @get:Rule
    val activityRule = ActivityScenarioRule (LoginActivity::class.java)

    //Проверим, что кнопка становится активной только после валидных логина и пароля
    @Test
    fun checkLoginButtonIsUsable(){
        LoginPage()
            .checkButtonIsNotEnabled()
            .setUserName("qwerty")
            .setUserPassword("asdfgh")
            .checkButtonIsEnabled()
    }

    //Проверим, что кнопка не становится активной только после невалидных логина и пароля
    @Test
    fun checkLoginButtonIsNotUsable(){
        LoginPage()
            .checkButtonIsNotEnabled()
            .setUserName(" ")
            .setUserPassword("asdf")
            .checkButtonIsNotEnabled()
    }

    //Проверим, что пользователю выводяться подсказки в полях ввода
    @Test
    fun checkStartHints(){
        LoginPage()
            .checkUsernameHint("Email")
            .checkPasswordHint("Password")
    }

    //Проверим, что кнопка правильно подписана (Тест пройдёт)
    @Test
    fun checkButtonText(){
        LoginPage()
            .checkButtonText("SIGN IN OR REGISTER")
    }

    //Проверим, что минимальная длина логина в нашей форме 1 символ
    @Test
    fun checkValidLoginLength(){
        LoginPage()
            .checkButtonIsNotEnabled()
            .setUserName("A")
            .setUserPassword("asdfgh")
            .checkButtonIsEnabled()
    }

    //Проверим, что минимальная длина пароля в нашей форме 5 символ
    @Test
    fun checkValidPasswordLength(){
        LoginPage()
            .checkButtonIsNotEnabled()
            .setUserName("A")
            .setUserPassword("asdfg")
            .checkButtonIsNotEnabled()
            .setUserPassword("h")
            .checkButtonIsEnabled()
    }

    ////Проверим, что при вводе в пробелов в поле логина появляется подсказка
    @Test
    fun checkHintLoginSpaces(){
        LoginPage()
            .checkButtonIsNotEnabled()
            .setUserName("   ")
            .setUserPassword("Asdfgh")
            .checkUsernameFailHint("Not a valid username")
            .checkButtonIsNotEnabled()
    }

    //Проверим, что при вводе в поле пароля 5 символов появляется подсказка
    @Test
    fun checkHintPasswordLength(){
        LoginPage()
            .checkButtonIsNotEnabled()
            .setUserName("A")
            .setUserPassword("Asdfg")
            .checkPasswordFailHint("Password must be >5 characters")
            .checkButtonIsNotEnabled()
    }
}