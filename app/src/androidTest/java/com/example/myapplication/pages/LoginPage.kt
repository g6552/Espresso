package com.example.myapplication.pages

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import com.example.myapplication.*

class LoginPage {

    fun checkButtonIsNotEnabled() : LoginPage {
        onView(withId(R.id.login)).check(matches(isNotEnabled()))
        return this
    }

    fun setUserName(name: String) : LoginPage {
        onView(withId(R.id.username)).perform(typeText(name))
        return this
    }

    fun setUserPassword(password: String) : LoginPage {
        onView(withId(R.id.password)).perform(typeText(password))
        return this
    }

    fun checkButtonIsEnabled() : LoginPage {
        onView(withId(R.id.login)).check(matches(isEnabled()))
        return this
    }

    fun checkButtonText(text: String) : LoginPage {
        onView(withId(R.id.login)).check(matches(withText(text)))
        return this
    }


    fun checkUsernameHint(hint: String) : LoginPage {
        onView(withId(R.id.username)).check(matches(withHint(hint)))
        return this
    }

    fun checkPasswordHint(hint: String) : LoginPage {
        onView(withId(R.id.password)).check(matches(withHint(hint)))
        return this
    }

    fun checkPasswordFailHint(hint: String) : LoginPage {
        onView(withId(R.id.password)).check(matches(withHint(hint)))
        return this
    }

    fun checkUsernameFailHint(hint: String) : LoginPage {
        onView(withId(R.id.username)).check(matches(withHint(hint)))
        return this
    }
}
